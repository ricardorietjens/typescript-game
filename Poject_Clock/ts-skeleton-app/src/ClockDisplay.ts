/// <reference path="NumberDisplay.ts" />

class ClockDisplay {
    private hours: NumberDisplay;
    private minutes: NumberDisplay;

    public constructor(hours: number, minutes: number) {
        this.minutes = new NumberDisplay(minutes, 59);
        this.hours = new NumberDisplay(hours, 23);
    }

    public tick(): void {
        this.minutes.increment()

        if (this.minutes.getValue() === 0) {
            this.hours.increment();
        }
    }

    public getTime(): string {
        if (this.minutes.getValue() < 10 && this.hours.getValue() < 10) {
            return `Het is momenteel 0${this.hours.getValue()} : 0${this.minutes.getValue()}`;
        } else if (this.hours.getValue() < 10) {
            return `Het is momenteel 0${this.hours.getValue()} : ${this.minutes.getValue()}`;
        } else if (this.minutes.getValue() < 10) {
            return `Het is momenteel ${this.hours.getValue()} : 0${this.minutes.getValue()}`;
        } else {
            return `Het is momenteel ${this.hours.getValue()} : ${this.minutes.getValue()}`
        }
    }

}