class NumberDisplay {
    constructor(value, limit) {
        this.value = value;
        this.limit = limit;
    }
    increment() {
        if (this.value == this.limit) {
            this.value = 0;
        }
        else {
            this.value++;
        }
    }
    getValue() {
        return this.value;
    }
}
class ClockDisplay {
    constructor(hours, minutes) {
        this.minutes = new NumberDisplay(minutes, 59);
        this.hours = new NumberDisplay(hours, 23);
    }
    tick() {
        this.minutes.increment();
        if (this.minutes.getValue() === 0) {
            this.hours.increment();
        }
    }
    getTime() {
        if (this.minutes.getValue() < 10 && this.hours.getValue() < 10) {
            return `Het is momenteel 0${this.hours.getValue()} : 0${this.minutes.getValue()}`;
        }
        else if (this.hours.getValue() < 10) {
            return `Het is momenteel 0${this.hours.getValue()} : ${this.minutes.getValue()}`;
        }
        else if (this.minutes.getValue() < 10) {
            return `Het is momenteel ${this.hours.getValue()} : 0${this.minutes.getValue()}`;
        }
        else {
            return `Het is momenteel ${this.hours.getValue()} : ${this.minutes.getValue()}`;
        }
    }
}
let clock = new ClockDisplay(13, 47);
for (let i = 0; i < 5000; i++) {
    console.log(clock.getTime());
    clock.tick();
}
//# sourceMappingURL=app.js.map