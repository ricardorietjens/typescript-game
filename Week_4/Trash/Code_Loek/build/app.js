class Asteroid {
    constructor(src, xCoordinate, yCoordinate) {
        this.src = src;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }
    update() {
    }
}
class Canvas {
    constructor(canvas, ctx) {
        this.canvas = canvas;
        this.ctx = ctx;
    }
    writeTextToCanvas(text, fontSize, xCoordinate, yCoordinate, color = "white", alignment = "center") {
        this.ctx.font = `${fontSize}px Minecraft`;
        this.ctx.fillStyle = color;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xCoordinate, yCoordinate);
    }
    writeImageToCanvas(src, xCoordinate, yCoordinate, deltaX = 0, deltaY = 0, loops = 1) {
        let element = document.createElement("img");
        element.src = src;
        for (let i = 0; i < loops; i++) {
            element.addEventListener("load", () => {
                xCoordinate += deltaX;
                yCoordinate += deltaY;
                this.ctx.drawImage(element, xCoordinate, yCoordinate);
            });
        }
    }
    writeButtonToCanvas(aCaption, aXpos = -1, aYpos = -1) {
        let buttonElement = new Image();
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";
        buttonElement.addEventListener("load", () => {
            let dx = aXpos;
            let dy = aYpos;
            if (dx < 0)
                dx = (this.canvas.width / 2 - buttonElement.width / 2);
            if (dy < 0)
                dy = (this.canvas.height) / 1.4 + buttonElement.height;
            let fontX = dx + ((buttonElement.width + aCaption.length - 18) / 2);
            let fontY = dy + (buttonElement.height - 12);
            this.ctx.drawImage(buttonElement, dx, dy);
            this.writeTextToCanvas(aCaption, 20, fontX, fontY, "blue");
        });
    }
    ;
}
class Player {
    constructor() {
    }
    update() {
    }
    eventCallBacks() {
    }
}
class Game {
    constructor() {
    }
    loop() {
    }
    eventHandlers() {
    }
    startScreen() {
    }
    levelScreen() {
    }
    titleScreen() {
    }
}
//# sourceMappingURL=app.js.map