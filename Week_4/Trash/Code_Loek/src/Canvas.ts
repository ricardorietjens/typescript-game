class Canvas {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D

    public constructor(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D) {
        // TODO
        this.canvas = canvas;
        this.ctx = ctx;
    }

    public writeTextToCanvas(
        text: string,
        fontSize: number,
        xCoordinate: number,
        yCoordinate: number,
        color: string = "white",
        alignment: CanvasTextAlign = "center"
    ) {
        this.ctx.font = `${fontSize}px Minecraft`;
        this.ctx.fillStyle = color;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xCoordinate, yCoordinate);
    }

    public writeImageToCanvas(
        src: string,
        xCoordinate: number,
        yCoordinate: number,
        deltaX: number = 0,
        deltaY: number = 0,
        loops: number = 1
    ) {
        // TODO
        let element = document.createElement("img");
        element.src = src;

        for (let i = 0; i < loops; i++) {
            element.addEventListener("load", () => {
                xCoordinate += deltaX;
                yCoordinate += deltaY;
                this.ctx.drawImage(element, xCoordinate, yCoordinate);
            });
        }
    }

    public writeButtonToCanvas(aCaption: string, aXpos: number = -1, aYpos: number = -1) {
        // copy content from the game.ts and make it error free
        // adjust for the different arguments as are available in the game.ts.
        let buttonElement = new Image();
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";

        buttonElement.addEventListener("load", (): void => {
            let dx = aXpos;
            let dy = aYpos;

            if (dx < 0) dx = (this.canvas.width / 2 - buttonElement.width / 2);
            if (dy < 0) dy = (this.canvas.height) / 1.4 + buttonElement.height;

            let fontX = dx + ((buttonElement.width + aCaption.length - 18) / 2);
            let fontY = dy + (buttonElement.height - 12)

            this.ctx.drawImage(buttonElement, dx, dy);
            this.writeTextToCanvas(aCaption, 20, fontX, fontY, "blue");

        });
    };
}
