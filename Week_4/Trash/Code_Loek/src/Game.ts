///<reference path="Asteroid.ts" />
///<reference path="Canvas.ts" />
///<reference path="Player.ts" />

class Game
{
    private state: string; // Which screen?
    private player: Player;
    private asteroids: Array<Asteroid>;
    private highScores: Array<number>; // Misschien een nieuwe klasse HighScore?

    public constructor()
    {
        // All properties
    }

    public loop()
    {
        // Oude "draw" functie. Je maakt je game aan, en vervolgens draai je alleen deze functie
    }

    public eventHandlers()
    {
        // Key up, down, etc
        // Click event
    }

    public startScreen()
    {
        // Start screen
    }

    public levelScreen()
    {
        // Level screen
    }

    public titleScreen()
    {
        // Title screen
    }
}
