class Canvas {
    constructor(canvas) {
        this.d_canvas = canvas;
        this.d_context = this.d_canvas.getContext('2d');
        console.log('in canvas constructor');
    }
    Clear() {
    }
    writeTextToCanvas(aText, aFontSize, aXpos, aYpos, aColor = "white", aAlignment = "center") {
        this.d_context.font = `${aFontSize}px Minecraft`;
        this.d_context.fillStyle = aColor;
        this.d_context.textAlign = aAlignment;
        this.d_context.fillText(aText, aXpos, aYpos);
    }
    writeImageFromFileToCanvas(aSrc, aXpos, aYpos) {
        let image = new Image();
        image.addEventListener('load', () => {
            this.d_context.drawImage(image, aXpos, aYpos);
        });
        image.src = aSrc;
    }
}
class Game {
    constructor() {
        const canvasElement = document.getElementById('canvas');
        this._canvas = new Canvas(canvasElement);
        this._player = new Player(canvasElement, './assets/images/player.png', 100, 100);
        this._zombie1 = new Zombie(canvasElement, './assets/images/Zombies/4ZombieFrontSPAWN.png', 10, 10);
        console.log('in game constructor');
        this.draw();
    }
    draw() {
        this._player.draw();
        this._zombie1.draw();
    }
}
window.addEventListener('load', init);
function init() {
    const ZombieGame = new Game();
}
class Entity {
    constructor(canvas, imageSource, xCoor, yCoor) {
        this._canvas = new Canvas(canvas);
        this._imageSrc = imageSource;
        this._xPos = xCoor;
        this._yPos = yCoor;
    }
    draw() {
        this._canvas.writeImageFromFileToCanvas(this._imageSrc, this._xPos, this._yPos);
    }
}
class Player extends Entity {
    constructor(canvas, imageSource, xCoor, yCoor) {
        super(canvas, imageSource, xCoor, yCoor);
    }
}
class Zombie extends Entity {
    constructor(canvas, imageSource, xCoor, yCoor) {
        super(canvas, imageSource, xCoor, yCoor);
    }
}
//# sourceMappingURL=app.js.map