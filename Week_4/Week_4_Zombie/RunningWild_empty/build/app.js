var RunningWild;
(function (RunningWild) {
    class App {
        constructor(aCanvas) {
            this.d_canvasHelper = RunningWild.CanvasHelper.Instance(aCanvas);
            this.d_canvasHelper.ChangeView(new RunningWild.MenuView());
        }
    }
    RunningWild.App = App;
})(RunningWild || (RunningWild = {}));
let init = function () {
    const myGame = new RunningWild.App(document.getElementById('canvas'));
};
window.addEventListener('load', init);
var RunningWild;
(function (RunningWild) {
    class ViewBase {
        constructor() {
            this.d_canvasHelper = RunningWild.CanvasHelper.Instance();
        }
        Render() {
            this.d_canvasHelper.Clear();
            this.RenderScreen();
        }
        BeforeExit() {
            this.Cleanup();
        }
    }
    RunningWild.ViewBase = ViewBase;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    let GameStates;
    (function (GameStates) {
        GameStates[GameStates["PAUSED"] = 0] = "PAUSED";
        GameStates[GameStates["STOPPED"] = 1] = "STOPPED";
        GameStates[GameStates["RUNNING"] = 2] = "RUNNING";
    })(GameStates || (GameStates = {}));
    class GameController {
        constructor() {
            this.d_canvasHelper = RunningWild.CanvasHelper.Instance();
            this.d_gameState = GameStates.STOPPED;
            this.d_score = 0;
            this.GameLoop = () => {
                this.d_canvasHelper.BeginUpdate();
                this.d_canvasHelper.writeTextToCanvas(`Score: ${this.d_score}`, 20, this.d_canvasHelper.GetWidth() - 150, 65, undefined, "right");
                this.d_canvasHelper.EndUpdate();
                if (this.d_gameState == GameStates.RUNNING) {
                }
            };
        }
        static Instance() {
            if (this.instance == null) {
                this.instance = new GameController();
            }
            return this.instance;
        }
        Start() {
            if (this.d_gameState == GameStates.PAUSED)
                return this.Resume();
            this.d_gameState = GameStates.RUNNING;
            this.GameLoop();
        }
        Stop() {
            this.d_gameState = GameStates.STOPPED;
        }
        Pause() {
            this.d_gameState = GameStates.PAUSED;
        }
        Resume() {
            this.d_gameState = GameStates.RUNNING;
            this.GameLoop();
        }
    }
    GameController.instance = null;
    RunningWild.GameController = GameController;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class ButtonAction {
        constructor(x, y, h, w, fn) {
            this.x = x;
            this.y = y;
            this.h = h;
            this.w = w;
            this.fn = fn;
        }
        ExecuteIfInArea(x, y) {
            if (x > this.x && x < this.x + this.w &&
                y > this.y && y < this.y + this.h) {
                this.fn();
            }
        }
    }
    RunningWild.ButtonAction = ButtonAction;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class CanvasHelper {
        constructor(aCanvas) {
            this.d_clickCommands = new Map();
            this.ChangeView = (aNewView = null) => {
                if (aNewView == null) {
                    return;
                }
                if (this.d_currentScreen != null) {
                    this.d_currentScreen.BeforeExit();
                }
                this.d_currentScreen = aNewView;
                this.d_currentScreen.Render();
            };
            this.d_canvas = aCanvas;
            this.d_canvas.width = window.innerWidth;
            this.d_canvas.height = window.innerHeight;
            this.d_context = this.d_canvas.getContext('2d');
            document.addEventListener('click', (event) => {
                this.OnClick(event);
            });
        }
        static Instance(aCanvas = null) {
            if (this.instance == null) {
                if (aCanvas == null) {
                    throw new DOMException("The first time the instance is created a Canvas must be given.");
                }
                this.instance = new CanvasHelper(aCanvas);
            }
            return this.instance;
        }
        OnClick(Event) {
            let X = Event.x;
            let Y = Event.y;
            this.d_clickCommands.forEach((value, key) => {
                value.ExecuteIfInArea(X, Y);
            });
        }
        Clear() {
            this.d_context.clearRect(0, 0, this.GetWidth(), this.GetHeight());
        }
        BeginUpdate() {
            this.d_context.save();
        }
        EndUpdate() {
            this.d_context.clip();
            this.d_context.restore();
        }
        GetCanvas() {
            return this.d_canvas;
        }
        GetCenter() {
            return { X: this.GetWidth() / 2, Y: this.GetHeight() / 2 };
        }
        GetHeight() {
            return this.d_canvas.height;
        }
        GetWidth() {
            return this.d_canvas.width;
        }
        UnregisterClickListener(fnName) {
            this.d_clickCommands.delete(fnName);
        }
        writeTextToCanvas(aText, aFontSize, aXpos, aYpos, aColor = "white", aAlignment = "center") {
            this.d_context.font = `${aFontSize}px Minecraft`;
            this.d_context.fillStyle = aColor;
            this.d_context.textAlign = aAlignment;
            this.d_context.fillText(aText, aXpos, aYpos);
        }
        writeImageFromFileToCanvas(aSrc, aXpos, aYpos) {
            let image = new Image();
            image.addEventListener('load', () => {
                this.d_context.drawImage(image, aXpos, aYpos);
            });
            image.src = aSrc;
        }
        writeImageToCanvas(aImage, aXpos, aYpos) {
            this.d_context.drawImage(aImage, aXpos, aYpos);
        }
        writeButtonToCanvas(aCaption, fnName, fn, aXpos = -1, aYpos = -1) {
            let buttonImage = new Image();
            buttonImage.src = "./assets/images/UI/buttonBlue.png";
            buttonImage.addEventListener('load', () => {
                let dx = aXpos;
                let dy = aYpos;
                if (dx < 0)
                    dx = (this.GetWidth() - buttonImage.width) / 2;
                if (dy < 0)
                    dy = this.GetHeight() / 2 + buttonImage.height;
                let fontX = dx + ((buttonImage.width + aCaption.length - 18) / 2);
                let fontY = dy + (buttonImage.height - 12);
                this.d_context.drawImage(buttonImage, dx, dy);
                this.writeTextToCanvas(aCaption, 20, fontX, fontY, '#000');
                if (fn != null) {
                    this.d_clickCommands.set(fnName, new RunningWild.ButtonAction(dx, dy, buttonImage.height, buttonImage.width, fn));
                }
            });
        }
    }
    CanvasHelper.instance = null;
    RunningWild.CanvasHelper = CanvasHelper;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class KeyboardHelper {
        constructor() {
            this.keyPressCallback = new Map();
            this.keyUpCallback = new Map();
            this.keyDownCallback = new Map();
            this.keyboardDown = (event) => {
                if (this.keyDownCallback.has(event.key)) {
                    event.preventDefault();
                    let callback = this.keyDownCallback.get(event.key);
                    if (callback != null) {
                        callback();
                    }
                }
            };
            this.keyboardUp = (event) => {
                if (this.keyUpCallback.has(event.key)) {
                    event.preventDefault();
                    let callback = this.keyUpCallback.get(event.key);
                    if (callback != null) {
                        callback();
                    }
                }
                if (this.keyPressCallback.has(event.key)) {
                    event.preventDefault();
                    let callback = this.keyPressCallback.get(event.key);
                    if (callback != null) {
                        callback();
                    }
                }
            };
            document.addEventListener('keydown', this.keyboardDown);
            document.addEventListener('keyup', this.keyboardUp);
        }
        static Instance() {
            if (this.instance == null) {
                this.instance = new KeyboardHelper();
            }
            return this.instance;
        }
        addKeyPressCallback(key, fn) {
            this.keyPressCallback.set(key, fn);
        }
        addKeyUpCallback(key, fn) {
            this.keyUpCallback.set(key, fn);
        }
        addKeyDownCallback(key, fn) {
            this.keyDownCallback.set(key, fn);
        }
        clearKeyCallbacks(key) {
            this.keyPressCallback.set(key, null);
            this.keyUpCallback.set(key, null);
            this.keyDownCallback.set(key, null);
        }
    }
    KeyboardHelper.instance = null;
    RunningWild.KeyboardHelper = KeyboardHelper;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class MathHelper {
        static randomNumber(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
    }
    RunningWild.MathHelper = MathHelper;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class GameView extends RunningWild.ViewBase {
        constructor() {
            super();
            this.score = 400;
            this.d_GameController = RunningWild.GameController.Instance();
        }
        RenderScreen() {
            this.d_GameController.Start();
        }
        Cleanup() {
        }
    }
    RunningWild.GameView = GameView;
})(RunningWild || (RunningWild = {}));
var RunningWild;
(function (RunningWild) {
    class MenuView extends RunningWild.ViewBase {
        constructor() {
            super();
            this.StartGame = () => {
                const center = this.d_canvasHelper.GetCenter();
                this.d_canvasHelper.ChangeView(new RunningWild.GameView());
            };
        }
        RenderScreen() {
            const centerCoordinate = this.d_canvasHelper.GetCenter();
            this.d_canvasHelper.writeTextToCanvas("Running Wild", 140, centerCoordinate.X, 150);
            this.d_canvasHelper.writeTextToCanvas("PRESS PLAY TO START", 40, centerCoordinate.X, centerCoordinate.Y - 20);
            this.d_canvasHelper.writeButtonToCanvas("Play", 'StartGameCommand', this.StartGame, undefined, centerCoordinate.Y + 200);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 50, centerCoordinate.Y + 40);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 20, centerCoordinate.Y + 40);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 10, centerCoordinate.Y + 40);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 40, centerCoordinate.Y + 40);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 50, centerCoordinate.Y + 80);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 20, centerCoordinate.Y + 80);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 10, centerCoordinate.Y + 80);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 40, centerCoordinate.Y + 80);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 50, centerCoordinate.Y + 120);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X - 20, centerCoordinate.Y + 120);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 10, centerCoordinate.Y + 120);
            this.d_canvasHelper.writeImageFromFileToCanvas("./assets/images/Player1.png", centerCoordinate.X + 40, centerCoordinate.Y + 120);
        }
        Cleanup() {
            this.d_canvasHelper.UnregisterClickListener('StartGameCommand');
        }
    }
    RunningWild.MenuView = MenuView;
})(RunningWild || (RunningWild = {}));
//# sourceMappingURL=app.js.map