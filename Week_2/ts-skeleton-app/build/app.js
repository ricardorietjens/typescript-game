class Game {
    constructor(canvasId) {
        this.player = "Player1";
        this.score = 400;
        this.lives = 3;
        this.playerShipImg = null;
        this.livesImg = null;
        this.shipXOffset = 50;
        this.shipYOffset = 37;
        this.draw = () => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.level_screen();
            if (this.leftPressed) {
                if (this.shipXOffset >= 0 - this.canvas.width / 2) {
                    this.shipXOffset -= 3;
                }
            }
            if (this.upPressed) {
                if (this.shipYOffset >= 100 - this.canvas.height / 2) {
                    this.shipYOffset -= 3;
                }
            }
            if (this.rightPressed) {
                if (this.shipXOffset <= this.canvas.width / 2 - 98) {
                    this.shipXOffset += 3;
                }
            }
            if (this.downPressed) {
                if (this.shipYOffset <= this.canvas.height / 2 - 75) {
                    this.shipYOffset += 3;
                }
            }
            window.requestAnimationFrame(this.draw);
        };
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext("2d");
        this.highscores = [
            {
                playerName: "Loek",
                score: 40000
            },
            {
                playerName: "Daan",
                score: 34000
            },
            {
                playerName: "Rimmert",
                score: 200
            }
        ];
        this.start_screen();
    }
    start_screen() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2;
        this.writeTextToCanvas("Asteroids", 140, horizontalCenter, 150);
        this.writeTextToCanvas("PRESS START TO PLAY", 40, horizontalCenter, verticalCenter - 20, "lightgreen");
        this.writeButtonToCanvas();
        this.writeImageToCanvas("./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_1.png", horizontalCenter - 50, verticalCenter + 40);
    }
    level_screen() {
        this.writePlayerLives(this.livesImg);
        this.writeTextToCanvas(`Score: ${this.score}`, 20, this.canvas.width / 1.1, this.canvas.height / 6, undefined, "right");
        this.writeImageToCanvas(this.playerShipImg, this.canvas.width / 2 + this.shipXOffset, this.canvas.height / 2 + this.shipYOffset);
    }
    drawPlayerLives() {
        const lifeImagePath = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
        let asteroidX = this.canvas.width / 25;
        let asteroidY = this.canvas.height / 8;
        console.log(lifeImagePath);
        for (let i = 0; i < this.lives; i++) {
            let element = document.createElement("img");
            element.src = lifeImagePath;
            element.addEventListener("load", () => {
                asteroidX += 40;
                this.ctx.drawImage(element, asteroidX, asteroidY);
            });
        }
        ;
    }
    writePlayerLives(src, xCoordinate = this.canvas.width / 25, yCoordinate = this.canvas.height / 8, deltaX = 0, deltaY = 0, loops = 1) {
        if (typeof src === "string") {
            let element = new Image();
            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            element.src = src;
        }
        else {
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 40, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 80, yCoordinate);
        }
    }
    drawRandomAsteroids() {
        let randomAmountOfAsteroids = this.randomNumber(1, 6);
        for (let index = 0; index < randomAmountOfAsteroids; index++) {
            let random = this.randomNumber(1, 10);
            let asteroidImage = new Image();
            asteroidImage.src = `./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_${random}.png`;
            asteroidImage.addEventListener('load', () => {
                let randomX = this.randomNumber(100, this.canvas.width - 100);
                let randomY = this.randomNumber(100, this.canvas.height - 100);
                this.ctx.drawImage(asteroidImage, randomX, randomY);
                console.log(`Het nummer van de Asteroid: ${random}`);
            });
        }
        console.log(`Het aantal random asteroids is ${randomAmountOfAsteroids}`);
    }
    title_screen() {
        const horizontalCenter = this.canvas.width / 2;
        let verticalCenter = this.canvas.height / 2;
        this.writeTextToCanvas(`${this.player} score is ${this.score}`, 60, horizontalCenter, verticalCenter - 100);
        this.writeTextToCanvas("HIGHSCORES", 40, horizontalCenter, verticalCenter);
        this.highscores.forEach((element, index) => {
            verticalCenter += 40;
            this.writeTextToCanvas(`${index + 1}: ${element.playerName} - ${element.score}`, 20, horizontalCenter, verticalCenter, "lightgreen");
        });
    }
    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
    writeTextToCanvas(text, fontSize, xCoordinate, yCoordinate, color = "white", alignment = "center") {
        this.ctx.font = `${fontSize}px Minecraft`;
        this.ctx.fillStyle = color;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xCoordinate, yCoordinate);
    }
    writeImageToCanvas(src, xCoordinate, yCoordinate, deltaX = 0, deltaY = 0, loops = 1) {
        if (typeof src === "string") {
            let element = new Image();
            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            element.src = src;
        }
        else {
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
        }
    }
    writeButtonToCanvas() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2.2;
        let buttonElement = document.createElement("img");
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";
        buttonElement.addEventListener("load", () => {
            this.ctx.drawImage(buttonElement, horizontalCenter - 111, verticalCenter + 219);
            this.writeTextToCanvas("Start", 20, horizontalCenter, verticalCenter + 245, "blue");
        });
        this.canvas.addEventListener("click", (event) => {
            if (event.x > horizontalCenter - 111 && event.x < horizontalCenter + 111) {
                if (event.y > verticalCenter + 219 && event.y < verticalCenter + 259) {
                    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                    window.addEventListener("keydown", (event) => this.keyDownHandler(event));
                    window.addEventListener("keyup", (event) => this.keyUpHandler(event));
                    this.playerShipImg = new Image();
                    this.livesImg = new Image();
                    this.playerShipImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
                    this.livesImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
                    this.playerShipImg.src = "./assets/images/SpaceShooterRedux/PNG/playerShip3_green.png";
                    this.livesImg.src = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
                }
            }
        });
    }
    keyDownHandler(event) {
        if (event.keyCode == 37) {
            this.leftPressed = true;
        }
        if (event.keyCode == 38) {
            this.upPressed = true;
        }
        if (event.keyCode == 39) {
            this.rightPressed = true;
        }
        if (event.keyCode == 40) {
            this.downPressed = true;
        }
    }
    keyUpHandler(event) {
        if (event.keyCode == 37) {
            this.leftPressed = false;
        }
        if (event.keyCode == 38) {
            this.upPressed = false;
        }
        if (event.keyCode == 39) {
            this.rightPressed = false;
        }
        if (event.keyCode == 40) {
            this.downPressed = false;
        }
    }
}
let init = function () {
    const Asteroids = new Game(document.getElementById('canvas'));
};
window.addEventListener('load', init);
//# sourceMappingURL=app.js.map