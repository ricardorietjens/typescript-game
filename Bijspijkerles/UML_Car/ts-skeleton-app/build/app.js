class Car {
    constructor(modelType, Doors, autoMaker) {
        this.modelType = modelType;
        this.Doors = Doors;
        this.autoMaker = autoMaker;
    }
    getRoadTrips() {
        return [new RoadTrip(), new RoadTrip()];
    }
    getModelType() {
        return this.modelType;
    }
    Radio() {
    }
    windshieldWiper() {
    }
    changeDirection() {
    }
}
class RoadTrip {
    route() {
    }
    detour() {
    }
    pottyBreak() {
    }
}
//# sourceMappingURL=app.js.map