let bondPlane = new Airplane("007", 2);

let jarno = new Passenger("Jarno", 45, bondPlane);
let ricardo = new Passenger("Ricardo", 45, bondPlane);
let jeffrey = new Passenger("Jeffrey", 45, bondPlane);

bondPlane.board(ricardo);
bondPlane.board(jarno);
bondPlane.board(jeffrey);

let passengers = bondPlane.getPassengers();
console.log(passengers);

console.log(jarno);
console.log(ricardo);
console.log(jeffrey);