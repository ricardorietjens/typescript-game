class Airplane {
    private typeAirplane: string
    private maxPassengers: number
    private passengers: Array<Passenger>;

    public constructor(typeAirplane: string, maxPassengers: number) {
        this.typeAirplane = typeAirplane;
        this.maxPassengers = maxPassengers;
        this.passengers = new Array<Passenger>();
    }

    public board(passenger: Passenger): void {
        this.passengers.push(passenger);
    }

    public getPassengers(): Array<Passenger> {
        return [
            new Passenger("Ricardo", 150, this),
            new Passenger("Jeffrey", 151, this),
            new Passenger("Jarno", 152, this)
        ]
    }
}
