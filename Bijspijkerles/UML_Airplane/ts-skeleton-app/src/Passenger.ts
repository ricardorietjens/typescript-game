/// <reference path= "Airplane.ts" />
class Passenger {
    private airplane: Airplane;
    private name: string;
    
    public constructor(name: string, seatNuber: number, airplane: Airplane) {
        this.name = name;
        this.airplane = airplane;
    }
}
