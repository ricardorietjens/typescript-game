class Airplane {
    constructor(typeAirplane, maxPassengers) {
        this.typeAirplane = typeAirplane;
        this.maxPassengers = maxPassengers;
        this.passengers = new Array();
    }
    board(passenger) {
        this.passengers.push(passenger);
    }
    getPassengers() {
        return [
            new Passenger("Ricardo", 150, this),
            new Passenger("Jeffrey", 151, this),
            new Passenger("Jarno", 152, this)
        ];
    }
}
class Passenger {
    constructor(name, seatNuber, airplane) {
        this.name = name;
        this.airplane = airplane;
    }
}
let bondPlane = new Airplane("007", 2);
let jarno = new Passenger("Jarno", 45, bondPlane);
let ricardo = new Passenger("Ricardo", 45, bondPlane);
let jeffrey = new Passenger("Jeffrey", 45, bondPlane);
bondPlane.board(ricardo);
bondPlane.board(jarno);
bondPlane.board(jeffrey);
let passengers = bondPlane.getPassengers();
console.log(passengers);
console.log(jarno);
console.log(ricardo);
console.log(jeffrey);
//# sourceMappingURL=app.js.map