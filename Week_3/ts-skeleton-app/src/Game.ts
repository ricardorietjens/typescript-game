class Game {
    //global attr for canvas
    //readonly attributes must be initialized in the constructor
    private readonly canvas: HTMLCanvasElement; // find the right type
    private readonly ctx: CanvasRenderingContext2D; // find the right type

    //some global player attributes
    private readonly player: string = "Player1";
    private readonly score: number = 400;
    private readonly lives: number = 3;
    private readonly highscores: Array<any>; //TODO: do not use 'any': write an interface!
    private playerShipImg: HTMLImageElement = null;
    private livesImg: HTMLImageElement = null;

    private leftPressed: boolean;
    private upPressed: boolean;
    private rightPressed: boolean;
    private downPressed: boolean;

    private shipXOffset: number = 50;
    private shipYOffset: number = 37;

    private d_currentView: ViewBase;

    public constructor(canvasId: HTMLCanvasElement) {
        //construct all canvas
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        //set the context of the canvas
        this.ctx = this.canvas.getContext("2d");

        this.highscores = [
            {
                playerName: "Loek",
                score: 40000
            },
            {
                playerName: "Daan",
                score: 34000
            },
            {
                playerName: "Rimmert",
                score: 200
            }
        ]

        // all screens: uncomment to activate 
        this.d_currentView = new MenuView(canvasId, this.ChangeView)
        this.d_currentView.Render();
        // this.start_screen();
        // this.level_screen();
        // this.title_screen();

    }

    private ChangeView(aNewView: ViewBase): void {
        this.d_currentView.BeforeExit();
        this.d_currentView = aNewView;
        this.d_currentView.Render();
    }

    //-------- Splash screen methods ------------------------------------
    /**
     * Function to initialize the splash screen
     */

    public start_screen() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2;

        //1. add 'Asteroids' text
        this.writeTextToCanvas("Asteroids", 140, horizontalCenter, 150);

        //2. add 'Press to play' text
        this.writeTextToCanvas("PRESS START TO PLAY", 40, horizontalCenter, verticalCenter - 20, "lightgreen");

        //3. add button with 'start' text
        this.writeButtonToCanvas();

        //4. add Asteroid image
        this.writeImageToCanvas(
            "./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_1.png",
            horizontalCenter - 50,
            verticalCenter + 40
        );
    }

    // }

    //-------- level screen methods -------------------------------------
    /**
     * Function to initialize the level screen
     */

    public level_screen() {
        //1. draw player lives
        // this.drawPlayerLives();
        this.writePlayerLives(this.livesImg);
        // this.drawPlayerLives();

        // 2. draw current score
        this.writeTextToCanvas(`Score: ${this.score}`, 20, this.canvas.width / 1.1, this.canvas.height / 6, undefined, "right");

        //3. draw random asteroids
        // this.drawRandomAsteroids();

        //4. draw player spaceship
        this.writeImageToCanvas(this.playerShipImg, this.canvas.width / 2 + this.shipXOffset, this.canvas.height / 2 + this.shipYOffset);


    }

    //1. load life images
    private drawPlayerLives() {
        const lifeImagePath: string = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
        let asteroidX: number = this.canvas.width / 25;
        let asteroidY: number = this.canvas.height / 8;
        console.log(lifeImagePath)

        for (let i = 0; i < this.lives; i++) {
            let element = document.createElement("img");
            element.src = lifeImagePath;

            element.addEventListener("load", () => {
                asteroidX += 40;
                this.ctx.drawImage(element, asteroidX, asteroidY);
                //console.log(`Het aantal levens: ${element.src}`)
            });
        };
    }

    public writePlayerLives(
        src: string | HTMLImageElement,
        xCoordinate: number = this.canvas.width / 25,
        yCoordinate: number = this.canvas.height / 8,
        deltaX: number = 0,
        deltaY: number = 0,
        loops: number = 1
    ) {
        if (typeof src === "string") {
            // Dit gebeurt er als het "src" parameter een
            // path is naar een plaatje.
            let element = new Image();

            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            // Heb je src NA de "load" eventListeners gezet, zodat er geen kans bestaat
            // Dat je plaatje al geladen is, voordat alle eventListeners gemaakt zijn.
            element.src = src;

        } else {
            // Dit gebeurt er als het "src" parameter een
            // al voorgaand gemaakt HTMLImageElement is.
            //Letterlijk de code uit je "load" eventListeners.
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 40, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 80, yCoordinate);

        }
    }

    // 3. draw random asteroids
    private drawRandomAsteroids() {
        // Een vijftal asteroids in het spel weergeven
        let randomAmountOfAsteroids = this.randomNumber(1, 6);
        for (let index = 0; index < randomAmountOfAsteroids; index++) {
            let random = this.randomNumber(1, 10);
            let asteroidImage = new Image();
            asteroidImage.src = `./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_${random}.png`;
            asteroidImage.addEventListener('load', () => {
                let randomX = this.randomNumber(100, this.canvas.width - 100);
                let randomY = this.randomNumber(100, this.canvas.height - 100);
                this.ctx.drawImage(asteroidImage, randomX, randomY);
                console.log(`Het nummer van de Asteroid: ${random}`)
            });
        }
        console.log(`Het aantal random asteroids is ${randomAmountOfAsteroids}`);
    }

    //-------- Title screen methods -------------------------------------

    /**
    * Function to initialize the title screen   
    */

    public title_screen() {
        const horizontalCenter = this.canvas.width / 2;
        let verticalCenter = this.canvas.height / 2;

        //1. draw your score
        this.writeTextToCanvas(`${this.player} score is ${this.score}`, 60, horizontalCenter, verticalCenter - 100);

        //2. draw all highscores
        this.writeTextToCanvas("HIGHSCORES", 40, horizontalCenter, verticalCenter);

        this.highscores.forEach((element, index) => {
            verticalCenter += 40;

            this.writeTextToCanvas(`${index + 1}: ${element.playerName} - ${element.score}`, 20, horizontalCenter, verticalCenter, "lightgreen");
        });
    }

    //-------Generic canvas functions ----------------------------------
    /**
     * Renders a random number between min and max
     * @param {number} min - minimal time
     * @param {number} max - maximal time
     * @param src - Full path to the file
     * @param xCoordinate - Horizontal coordinate in pixels
     * @param yCoordinate - Vertical coordinate in pixels
     * @param deltaX - Amount of offset pixels on horizontal axis every loop
     * @param deltaY - Amount of offset pixels on vertical axis every loop
     * @param loops - Amount of images that should be written
  */

    public randomNumber(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }

    /**
     * Writes text to the canvas
     * @param text - Text to write
     * @param fontSize - Font size in pixels
     * @param xCoordinate - Horizontal coordinate in pixels
     * @param yCoordinate - Vertical coordinate in pixels
     * @param alignment - Where to align the text
     * @param color - The color of the text
     */

    public writeTextToCanvas(
        text: string,
        fontSize: number,
        xCoordinate: number,
        yCoordinate: number,
        color: string = "white",
        alignment: CanvasTextAlign = "center"
    ) {
        this.ctx.font = `${fontSize}px Minecraft`;
        this.ctx.fillStyle = color;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xCoordinate, yCoordinate);
    }

    public writeImageToCanvas(
        src: string | HTMLImageElement,
        xCoordinate: number,
        yCoordinate: number,
        deltaX: number = 0,
        deltaY: number = 0,
        loops: number = 1
    ) {
        if (typeof src === "string") {
            // Dit gebeurt er als het "src" parameter een
            // path is naar een plaatje.
            let element = new Image();

            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            // Heb je src NA de "load" eventListeners gezet, zodat er geen kans bestaat
            // Dat je plaatje al geladen is, voordat alle eventListeners gemaakt zijn.
            element.src = src;
        } else {
            // Dit gebeurt er als het "src" parameter een
            // al voorgaand gemaakt HTMLImageElement is.

            //Letterlijk de code uit je "load" eventListeners.
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
        }
    }

    public writeButtonToCanvas() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2.2;

        let buttonElement = document.createElement("img");
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";

        buttonElement.addEventListener("load", () => {
            this.ctx.drawImage(buttonElement, horizontalCenter - 111, verticalCenter + 219);
            this.writeTextToCanvas("Start", 20, horizontalCenter, verticalCenter + 245, "blue");
        });

        this.canvas.addEventListener("click", (event: MouseEvent) => {
            if (event.x > horizontalCenter - 111 && event.x < horizontalCenter + 111) {
                if (event.y > verticalCenter + 219 && event.y < verticalCenter + 259) {
                    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                    //this.level_screen();
                    window.addEventListener("keydown", (event) => this.keyDownHandler(event));
                    window.addEventListener("keyup", (event) => this.keyUpHandler(event));

                    // Maak get HTMLImageElement aan, voordat het in de loop gebruikt wordt.
                    this.playerShipImg = new Image();
                    this.livesImg = new Image();

                    // Start de "draw" loop, nadat het benodigde HTMLImageElement af is 
                    this.playerShipImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
                    this.livesImg.addEventListener("load", () => window.requestAnimationFrame(this.draw))

                    // Image source
                    this.playerShipImg.src = "./assets/images/SpaceShooterRedux/PNG/playerShip3_green.png";
                    this.livesImg.src = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
                }
            }
        });

    }


    //-------- Animation screen methods -------------------------------------

    /**
    * Function to initialize the animation screen   
    */

    private draw = () => {
        // Clear your canvas
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // Levelscreen aanroepen
        this.level_screen()
        if (this.leftPressed) {
            if (this.shipXOffset >= 0 - this.canvas.width / 2) {
                this.shipXOffset -= 3;
            }
        }

        if (this.upPressed) {
            if (this.shipYOffset >= 100 - this.canvas.height / 2) {
                this.shipYOffset -= 3;
            }
        }

        if (this.rightPressed) {
            if (this.shipXOffset <= this.canvas.width / 2 - 98) {
                this.shipXOffset += 3;
            }
        }

        if (this.downPressed) {
            if (this.shipYOffset <= this.canvas.height / 2 - 75) {
                this.shipYOffset += 3;
            }
        }

        //Vraagt aan om deze methode uit te voeren bij het maken van de volgende frame.
        window.requestAnimationFrame(this.draw);
    }

    private keyDownHandler(event: KeyboardEvent) {
        if (event.keyCode == 37) {
            this.leftPressed = true;
        }
        if (event.keyCode == 38) {
            this.upPressed = true;
        }
        if (event.keyCode == 39) {
            this.rightPressed = true;
        }
        if (event.keyCode == 40) {
            this.downPressed = true;
        }
    }

    private keyUpHandler(event: KeyboardEvent) {
        if (event.keyCode == 37) {
            this.leftPressed = false;
        }
        if (event.keyCode == 38) {
            this.upPressed = false;
        }
        if (event.keyCode == 39) {
            this.rightPressed = false;
        }
        if (event.keyCode == 40) {
            this.downPressed = false;
        }
    }
}


//this will get an HTML element. I cast this element in de appropriate type using <>
let init = function () {
    const Asteroids = new Game(<HTMLCanvasElement>document.getElementById('canvas'));
};

//add loadlistener for custom font types
window.addEventListener('load', init);