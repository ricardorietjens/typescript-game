/// <reference path="../base/ViewBase.ts"/>

class MenuView extends ViewBase {
    /**
     * Constructor
     * Creates the object and initializes the members
     * @param {HTMLCanvasElement} aCanvas - the canvas where to render to
     * @param aChangeViewCallback -
     */
    public constructor(aCanvas: HTMLCanvasElement, aChangeViewCallback: (aNewView: ViewBase) => void) {
        // Aanroepen constructor ViewBase.ts
        super(aCanvas, aChangeViewCallback);
    }

    protected HandleClick = (aXpos: number, aYpos: number): void => {
        // get the centerCoordinate
        const center = this.d_canvasHelper.GetCenter();

        //1. add 'Asteroids' text
        this.d_canvasHelper.writeTextToCanvas("Asteroids", 140, center.X, 150);

        //2. add 'Press to play' text
        this.d_canvasHelper.writeTextToCanvas("PRESS START TO PLAY", 40, center.X, center.Y - 20, "lightgreen");

        //3. add button with 'start' text
        this.d_canvasHelper.writeButtonToCanvas("Start");

        //4. add Asteroid image
        this.d_canvasHelper.writeImageToCanvas(
            "./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_1.png",
            center.X - 50,
            center.Y + 40
        );

        if (aXpos > center.X - 111 && aXpos < center.X + 111) {
            if (aYpos > center.Y + 219 && aYpos < center.Y + 259) {
                // clear the canvas
                //this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                // change the View << is explained tomorrow
                //this.d_changeViewCallback(new GameView(this.d_canvasHelper.canvas, this.d_changeViewCallback));
            }
        }
    }

    protected RenderScreen(): void {
        // copy and modify the code from start_screen from the game.ts

    }

}