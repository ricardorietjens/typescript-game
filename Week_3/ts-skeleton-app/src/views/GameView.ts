/// <reference path="../base/ViewBase.ts"/>
/// <reference path="../views/MenuView.ts"/>

class GameView extends ViewBase {
    protected HandleClick(X: number, Y: number): void {
        throw new Error("Method not implemented.");
    }

    protected RenderScreen(): void {
        throw new Error("Method not implemented.");
    }
    
    //some global player attributes
    // these will not stay here
    private readonly player: string = "Player1";
    private readonly score: number = 400;
    private readonly lives: number = 3;
}