class CanvasHelper {

    public readonly canvas: HTMLCanvasElement;
    public readonly ctx: CanvasRenderingContext2D; //this was a bit tricky to find
    private playerShipImg: HTMLImageElement = null;
    private livesImg: HTMLImageElement = null;

    /**
     * constructor
     * @AccessModifier {public}
     * Clears the canvas
     * @param {HTMLCanvasElement} aCanvas - the canvas to help with
     */
    public constructor(aCanvas: HTMLCanvasElement) {
        // bind the passed argument to the local member
        this.canvas = aCanvas;

        //set the context of the canvas
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        // get the context from the canvas
        this.ctx = this.canvas.getContext("2d");
        ;
    }

    /**
     * RegisterOnClick
     * @AccessModifier {public}
     * Clears the canvas
     * @param aCallBack -
     */
    public RegisterOnClick(aCallBack: (x_axis: number, y_axis: number) => void) {
        // register an event listener to handle click events
        this.canvas.addEventListener('click', (aEvent: MouseEvent) => {
            // when this event is handles call the local OnClick method.
            aCallBack(aEvent.x, aEvent.y);
        });
    }

    /**
     * Clear
     * @AccessModifier {public}
     * Clears the canvas
     */
    public Clear(): void {
        // clear the screen
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    /**
     * GetCenter
     * @AccessModifier {public}
     * returns the center coordinate
     */
    public GetCenter(): { X: number, Y: number } {
        // return the center as a valid return
        return {
            X: (this.GetWidth() / 2),
            Y: (this.GetHeight() / 2)
        };
    }

    /**
     * GetHeight
     * @AccessModifier {public}
     * returns Height of the canvas
     */
    public GetHeight(): number {
        // return the height of te canvas
        return this.canvas.height;
    }

    /**
     * GetWidth
     * @AccessModifier {public}
     * returns the Width of the canvas
     */
    public GetWidth(): number {
        // return the width of the canvas
        return this.canvas.width;
    }

    /**
     * writeTextToCanvas
     * @AccessModifier {public}
     * Handles the internal redirection of the click event.
     * @param {string} aText -
     * @param {number} aFontSize -
     * @param {number} aXpos -
     * @param {number} aYpos -
     * @param {string} aColor -
     * @param {CanvasTextAlign} aAlignment -
     */
    public writeTextToCanvas(
        aText: string,
        aFontSize: number,
        aXpos: number,
        aYpos: number,
        aColor: string = "white",
        aAlignment: CanvasTextAlign = "center") {

        // copy content from the game.ts and make it error free
        this.ctx.font = `${aFontSize}px Minecraft`;
        this.ctx.fillStyle = aColor;
        this.ctx.textAlign = aAlignment;
        this.ctx.fillText(aText, aXpos, aYpos);
    }

    /**
     * writeTextToCanvas
     * @AccessModifier {public}
     * Handles the internal redirection of the click event.
     * @param {string} aSrc - the source of the resource
     * @param {number} aXpos - the x axis value of the coordinate
     * @param {number} aYpos - the y axis value of the coordinate
     */
    public writeImageToCanvas(aSrc: string,
        aXpos: number,
        aYpos: number,
        loops: number = 1
    ) {
        // copy content from the game.ts and make it error free
        // keep in mind that we do not support the printing of multiple side-by-side images
        // as it does in the game.ts
        let element = new Image();
        element.src = aSrc
        element.addEventListener("load", () => {
            this.ctx.drawImage(element, aXpos, aYpos);
        });
    }

    /**
    * writeButtonToCanvas
    * @AccessModifier {public}
    * Creates a button with a given text
    * @param {string} aCaption - the caption to write
    * @param {number} aXpos - the left top x position of the button
    * @param {number} aYpos - the left top y position of the button
    */
    public writeButtonToCanvas(aCaption: string, aXpos: number = -1, aYpos: number = -1) {
        // copy content from the game.ts and make it error free
        // adjust for the different arguments as are available in the game.ts.
        let buttonElement = new Image();
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";

        buttonElement.addEventListener("load", (): void => {
            let dx = aXpos;
            let dy = aYpos;

            if (dx < 0) dx = (this.GetWidth() / 2 - buttonElement.width / 2);
            if (dy < 0) dy = this.GetHeight() / 1.4 + buttonElement.height;

            let fontX = dx + ((buttonElement.width + aCaption.length - 18) / 2);
            let fontY = dy + (buttonElement.height - 12)

            this.ctx.drawImage(buttonElement, dx, dy);
            this.writeTextToCanvas(aCaption, 20, fontX, fontY, "blue");

        });
    };
}

/**
* this.canvas.addEventListener("click", (event: MouseEvent) => {
*     if (event.x > aXpos - 111 && event.x < aXpos + 111) {
*         if (event.y > aYpos + 219 && event.y < aYpos + 259) {
*             this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
*             this.level_screen();
*             window.addEventListener("keydown", (event) => this.keyDownHandler(event));
*             window.addEventListener("keyup", (event) => this.keyUpHandler(event));
*
*             Maak get HTMLImageElement aan, voordat het in de loop gebruikt wordt.
*             this.playerShipImg = new Image();
*             this.livesImg = new Image();
*
*             Start de "draw" loop, nadat het benodigde HTMLImageElement af is 
*             this.playerShipImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
*             this.livesImg.addEventListener("load", () => window.requestAnimationFrame(this.draw))
*
*             Image source
*             this.playerShipImg.src = "./assets/images/SpaceShooterRedux/PNG/playerShip3_green.png";
*             this.livesImg.src = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
*         }
*     }
* });
*/
