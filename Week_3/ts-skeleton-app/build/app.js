class Game {
    constructor(canvasId) {
        this.player = "Player1";
        this.score = 400;
        this.lives = 3;
        this.playerShipImg = null;
        this.livesImg = null;
        this.shipXOffset = 50;
        this.shipYOffset = 37;
        this.draw = () => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.level_screen();
            if (this.leftPressed) {
                if (this.shipXOffset >= 0 - this.canvas.width / 2) {
                    this.shipXOffset -= 3;
                }
            }
            if (this.upPressed) {
                if (this.shipYOffset >= 100 - this.canvas.height / 2) {
                    this.shipYOffset -= 3;
                }
            }
            if (this.rightPressed) {
                if (this.shipXOffset <= this.canvas.width / 2 - 98) {
                    this.shipXOffset += 3;
                }
            }
            if (this.downPressed) {
                if (this.shipYOffset <= this.canvas.height / 2 - 75) {
                    this.shipYOffset += 3;
                }
            }
            window.requestAnimationFrame(this.draw);
        };
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext("2d");
        this.highscores = [
            {
                playerName: "Loek",
                score: 40000
            },
            {
                playerName: "Daan",
                score: 34000
            },
            {
                playerName: "Rimmert",
                score: 200
            }
        ];
        this.d_currentView = new MenuView(canvasId, this.ChangeView);
        this.d_currentView.Render();
    }
    ChangeView(aNewView) {
        this.d_currentView.BeforeExit();
        this.d_currentView = aNewView;
        this.d_currentView.Render();
    }
    start_screen() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2;
        this.writeTextToCanvas("Asteroids", 140, horizontalCenter, 150);
        this.writeTextToCanvas("PRESS START TO PLAY", 40, horizontalCenter, verticalCenter - 20, "lightgreen");
        this.writeButtonToCanvas();
        this.writeImageToCanvas("./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_1.png", horizontalCenter - 50, verticalCenter + 40);
    }
    level_screen() {
        this.writePlayerLives(this.livesImg);
        this.writeTextToCanvas(`Score: ${this.score}`, 20, this.canvas.width / 1.1, this.canvas.height / 6, undefined, "right");
        this.writeImageToCanvas(this.playerShipImg, this.canvas.width / 2 + this.shipXOffset, this.canvas.height / 2 + this.shipYOffset);
    }
    drawPlayerLives() {
        const lifeImagePath = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
        let asteroidX = this.canvas.width / 25;
        let asteroidY = this.canvas.height / 8;
        console.log(lifeImagePath);
        for (let i = 0; i < this.lives; i++) {
            let element = document.createElement("img");
            element.src = lifeImagePath;
            element.addEventListener("load", () => {
                asteroidX += 40;
                this.ctx.drawImage(element, asteroidX, asteroidY);
            });
        }
        ;
    }
    writePlayerLives(src, xCoordinate = this.canvas.width / 25, yCoordinate = this.canvas.height / 8, deltaX = 0, deltaY = 0, loops = 1) {
        if (typeof src === "string") {
            let element = new Image();
            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            element.src = src;
        }
        else {
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 40, yCoordinate);
            this.ctx.drawImage(src, xCoordinate + 80, yCoordinate);
        }
    }
    drawRandomAsteroids() {
        let randomAmountOfAsteroids = this.randomNumber(1, 6);
        for (let index = 0; index < randomAmountOfAsteroids; index++) {
            let random = this.randomNumber(1, 10);
            let asteroidImage = new Image();
            asteroidImage.src = `./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_${random}.png`;
            asteroidImage.addEventListener('load', () => {
                let randomX = this.randomNumber(100, this.canvas.width - 100);
                let randomY = this.randomNumber(100, this.canvas.height - 100);
                this.ctx.drawImage(asteroidImage, randomX, randomY);
                console.log(`Het nummer van de Asteroid: ${random}`);
            });
        }
        console.log(`Het aantal random asteroids is ${randomAmountOfAsteroids}`);
    }
    title_screen() {
        const horizontalCenter = this.canvas.width / 2;
        let verticalCenter = this.canvas.height / 2;
        this.writeTextToCanvas(`${this.player} score is ${this.score}`, 60, horizontalCenter, verticalCenter - 100);
        this.writeTextToCanvas("HIGHSCORES", 40, horizontalCenter, verticalCenter);
        this.highscores.forEach((element, index) => {
            verticalCenter += 40;
            this.writeTextToCanvas(`${index + 1}: ${element.playerName} - ${element.score}`, 20, horizontalCenter, verticalCenter, "lightgreen");
        });
    }
    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
    writeTextToCanvas(text, fontSize, xCoordinate, yCoordinate, color = "white", alignment = "center") {
        this.ctx.font = `${fontSize}px Minecraft`;
        this.ctx.fillStyle = color;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xCoordinate, yCoordinate);
    }
    writeImageToCanvas(src, xCoordinate, yCoordinate, deltaX = 0, deltaY = 0, loops = 1) {
        if (typeof src === "string") {
            let element = new Image();
            for (let i = 0; i < loops; i++) {
                element.addEventListener("load", () => {
                    xCoordinate += deltaX;
                    yCoordinate += deltaY;
                    this.ctx.drawImage(element, xCoordinate, yCoordinate);
                });
            }
            element.src = src;
        }
        else {
            xCoordinate += deltaX;
            yCoordinate += deltaY;
            this.ctx.drawImage(src, xCoordinate, yCoordinate);
        }
    }
    writeButtonToCanvas() {
        const horizontalCenter = this.canvas.width / 2;
        const verticalCenter = this.canvas.height / 2.2;
        let buttonElement = document.createElement("img");
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";
        buttonElement.addEventListener("load", () => {
            this.ctx.drawImage(buttonElement, horizontalCenter - 111, verticalCenter + 219);
            this.writeTextToCanvas("Start", 20, horizontalCenter, verticalCenter + 245, "blue");
        });
        this.canvas.addEventListener("click", (event) => {
            if (event.x > horizontalCenter - 111 && event.x < horizontalCenter + 111) {
                if (event.y > verticalCenter + 219 && event.y < verticalCenter + 259) {
                    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                    window.addEventListener("keydown", (event) => this.keyDownHandler(event));
                    window.addEventListener("keyup", (event) => this.keyUpHandler(event));
                    this.playerShipImg = new Image();
                    this.livesImg = new Image();
                    this.playerShipImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
                    this.livesImg.addEventListener("load", () => window.requestAnimationFrame(this.draw));
                    this.playerShipImg.src = "./assets/images/SpaceShooterRedux/PNG/playerShip3_green.png";
                    this.livesImg.src = "./assets/images/SpaceShooterRedux/PNG/UI/playerLife3_green.png";
                }
            }
        });
    }
    keyDownHandler(event) {
        if (event.keyCode == 37) {
            this.leftPressed = true;
        }
        if (event.keyCode == 38) {
            this.upPressed = true;
        }
        if (event.keyCode == 39) {
            this.rightPressed = true;
        }
        if (event.keyCode == 40) {
            this.downPressed = true;
        }
    }
    keyUpHandler(event) {
        if (event.keyCode == 37) {
            this.leftPressed = false;
        }
        if (event.keyCode == 38) {
            this.upPressed = false;
        }
        if (event.keyCode == 39) {
            this.rightPressed = false;
        }
        if (event.keyCode == 40) {
            this.downPressed = false;
        }
    }
}
let init = function () {
    const Asteroids = new Game(document.getElementById('canvas'));
};
window.addEventListener('load', init);
class CanvasHelper {
    constructor(aCanvas) {
        this.playerShipImg = null;
        this.livesImg = null;
        this.canvas = aCanvas;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext("2d");
        ;
    }
    RegisterOnClick(aCallBack) {
        this.canvas.addEventListener('click', (aEvent) => {
            aCallBack(aEvent.x, aEvent.y);
        });
    }
    Clear() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
    GetCenter() {
        return {
            X: (this.GetWidth() / 2),
            Y: (this.GetHeight() / 2)
        };
    }
    GetHeight() {
        return this.canvas.height;
    }
    GetWidth() {
        return this.canvas.width;
    }
    writeTextToCanvas(aText, aFontSize, aXpos, aYpos, aColor = "white", aAlignment = "center") {
        this.ctx.font = `${aFontSize}px Minecraft`;
        this.ctx.fillStyle = aColor;
        this.ctx.textAlign = aAlignment;
        this.ctx.fillText(aText, aXpos, aYpos);
    }
    writeImageToCanvas(aSrc, aXpos, aYpos, loops = 1) {
        let element = new Image();
        element.src = aSrc;
        element.addEventListener("load", () => {
            this.ctx.drawImage(element, aXpos, aYpos);
        });
    }
    writeButtonToCanvas(aCaption, aXpos = -1, aYpos = -1) {
        let buttonElement = new Image();
        buttonElement.src = "./assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";
        buttonElement.addEventListener("load", () => {
            let dx = aXpos;
            let dy = aYpos;
            if (dx < 0)
                dx = (this.GetWidth() / 2 - buttonElement.width / 2);
            if (dy < 0)
                dy = this.GetHeight() / 1.4 + buttonElement.height;
            let fontX = dx + ((buttonElement.width + aCaption.length - 18) / 2);
            let fontY = dy + (buttonElement.height - 12);
            this.ctx.drawImage(buttonElement, dx, dy);
            this.writeTextToCanvas(aCaption, 20, fontX, fontY, "blue");
        });
    }
    ;
}
class ViewBase {
    constructor(aCanvas, aChangeViewCallback) {
        this.d_alive = true;
        this.OnClick = (aXaxis, aYaxis) => {
            if (!this.d_alive)
                return;
            this.HandleClick(aXaxis, aYaxis);
        };
        this.d_canvasHelper = new CanvasHelper(aCanvas);
        this.d_changeViewCallback = aChangeViewCallback;
        this.d_canvasHelper.RegisterOnClick(this.OnClick);
    }
    Render() {
    }
    BeforeExit() {
        this.d_alive = false;
    }
}
class MathHelper {
    static randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
}
class MenuView extends ViewBase {
    constructor(aCanvas, aChangeViewCallback) {
        super(aCanvas, aChangeViewCallback);
        this.HandleClick = (aXpos, aYpos) => {
            const center = this.d_canvasHelper.GetCenter();
            this.d_canvasHelper.writeTextToCanvas("Asteroids", 140, center.X, 150);
            this.d_canvasHelper.writeTextToCanvas("PRESS START TO PLAY", 40, center.X, center.Y - 20, "lightgreen");
            this.d_canvasHelper.writeButtonToCanvas("Start");
            this.d_canvasHelper.writeImageToCanvas("./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_1.png", center.X - 50, center.Y + 40);
            if (aXpos > center.X - 111 && aXpos < center.X + 111) {
                if (aYpos > center.Y + 219 && aYpos < center.Y + 259) {
                }
            }
        };
    }
    RenderScreen() {
    }
}
class GameView extends ViewBase {
    constructor() {
        super(...arguments);
        this.player = "Player1";
        this.score = 400;
        this.lives = 3;
    }
    HandleClick(X, Y) {
        throw new Error("Method not implemented.");
    }
    RenderScreen() {
        throw new Error("Method not implemented.");
    }
}
//# sourceMappingURL=app.js.map